FROM openjdk:alpine
COPY . /opt
WORKDIR /opt/minecraft

RUN apk add --no-cache bash
RUN apk add --no-cache py-pip
RUN pip install mcstatus

RUN wget https://media.forgecdn.net/files/3432/932/SIMPLE+SERVER+FILES+1.7.10.zip
RUN unzip SIMPLE+SERVER+FILES+1.7.10.zip
RUN rm SIMPLE+SERVER+FILES+1.7.10.zip

RUN cp /opt/eula.txt /opt/minecraft

RUN chmod 700 startserver.sh

HEALTHCHECK --interval=30s --timeout=5s --retries=5 \
   CMD mcstatus localhost ping || exit 1

EXPOSE 25565
ENTRYPOINT ["/opt/minecraft/startserver.sh"]

